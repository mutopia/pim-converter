package au.com.mutopia.util;

import lombok.extern.log4j.Log4j;
import org.bimserver.client.BimServerClient;
import org.bimserver.client.BimServerClientFactory;
import org.bimserver.client.ChannelConnectionException;
import org.bimserver.client.json.JsonBimServerClientFactory;
import org.bimserver.shared.UsernamePasswordAuthenticationInfo;
import org.bimserver.shared.exceptions.ServiceException;

/**
 * A class for handling connections to Bim Server.
 */
@Log4j
public class BimServerAuthenticator {
  /**
   * @param username
   * @param password
   * @param host The host address of the BimServer.
   * @return {@link org.bimserver.client.BimServerClient} that is connected to BimServer at given host address,
   * username and password.
   */
  public BimServerClient connectToBimServer(String username, String password, String host) {
    BimServerClientFactory factory = new JsonBimServerClientFactory(host);
    BimServerClient bimServerClient = null;
    // Create a new BimServerClient with authentication.
    try {
      bimServerClient = factory.create(new UsernamePasswordAuthenticationInfo(username, password));
    } catch (ServiceException | ChannelConnectionException e) {
      log.error("Failed to create BIMserver client", e);
    }
    return bimServerClient;
  }
}
