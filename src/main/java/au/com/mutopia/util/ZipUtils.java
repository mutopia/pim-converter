package au.com.mutopia.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;

/**
 * A utility for working with .zip files.
 */
@Log4j
public class ZipUtils {
  public ZipUtils() {}

  /**
   * Unzip all contents of the given ZIP archive to the given directory.
   *
   * @return A list of extracted files,
   */
  public ArrayList<File> unzip(File archive, File outputDir) throws IOException {
    ArrayList<File> files = new ArrayList<File>();
    ZipFile zipfile = new ZipFile(archive);
    log.debug("Unzipping " + archive.getName());
    for (Enumeration<? extends ZipEntry> e = zipfile.entries(); e.hasMoreElements();) {
      ZipEntry entry = e.nextElement();
      log.debug("Zip entry " + entry);
      files.add(unzipEntryToFile(zipfile, entry, outputDir));
    }
    return files;
  }

  /**
   * Unzip files inside the given ZIP archive as a map of their contents.
   *
   * @return A list of extracted files,
   */
  public Map<String, byte[]> unzip(File archive) throws IOException {
    // TODO refactor with above function
    Map<String, byte[]> files = new HashMap<>();
    ZipFile zipfile = new ZipFile(archive);
    log.debug("Unzipping " + archive.getName());
    for (Enumeration<? extends ZipEntry> e = zipfile.entries(); e.hasMoreElements();) {
      ZipEntry entry = e.nextElement();
      log.debug("Zip entry " + entry);
      byte[] content = unzipEntryToBytes(zipfile, entry);
      files.put(entry.getName(), content);
    }
    return files;
  }

  /**
   * Unzip an entry of the ZIP archive to a directory.
   *
   * @return The extracted file.
   */
  private File unzipEntryToFile(ZipFile zipfile, ZipEntry entry, File outputDir) throws IOException {
    if (entry.isDirectory()) {
      createDir(new File(outputDir, entry.getName()));
      return null;
    }
    File outputFile = new File(outputDir, entry.getName());
    if (!outputFile.getParentFile().exists()) {
      createDir(outputFile.getParentFile());
    }
    BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
    unzipEntryStream(zipfile, entry, outputStream);
    return outputFile;
  }

  /**
   * Unzip an entry of the ZIP archive.
   *
   * @return The extracted content of the {@link java.util.zip.ZipEntry}.
   */
  private byte[] unzipEntryToBytes(ZipFile zipfile, ZipEntry entry) throws IOException {
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    BufferedOutputStream outputStream = new BufferedOutputStream(byteStream);
    unzipEntryStream(zipfile, entry, outputStream);
    outputStream.flush();
    return byteStream.toByteArray();
  }

  /**
   * Unzips an entry of the ZIP archive to an {@link java.io.OutputStream}.
   */
  private void unzipEntryStream(ZipFile zipfile, ZipEntry entry, OutputStream outputStream)
      throws IOException {
    log.debug("Extracting: " + entry);
    BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
    try {
      IOUtils.copy(inputStream, outputStream);
    } finally {
      outputStream.close();
      inputStream.close();
    }
  }

  /**
   * Create a directory if it does not exist.
   */
  private void createDir(File dir) {
    log.debug("Creating dir " + dir.getName());
    if (!dir.mkdirs()) throw new RuntimeException("Can not create dir " + dir);
  }
}
