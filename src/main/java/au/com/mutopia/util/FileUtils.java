package au.com.mutopia.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class FileUtils {
  /**
   * The directory used to store temporary files.
   */
  public static final String TEMP_DIR = System.getProperty("java.io.tmpdir");

  public File createTempFile(String fileName) throws IOException {
    // Generate a subdirectory to allow using the given filename without a random string.
    File subDir = createTempDir();
    return new File(subDir, fileName);
  }

  /**
   * Creates a temporary file given the file name and content.
   *
   * @param fileName The name of the file to be created.
   * @param contents The contents of the file.
   * @return The temporary file created.
   * @throws java.io.IOException
   */
  public File createTemporaryFileWithContent(String fileName, byte[] contents)
      throws IOException {
    File tmpFile = createTempFile(fileName);
    FileOutputStream out = new FileOutputStream(tmpFile);
    if (contents != null) {
      out.write(contents);
    }
    out.flush();
    out.close();
    return tmpFile;
  }

  public File createTempDir() {
    String subDirName = UUID.randomUUID().toString();
    File subDir = new File(TEMP_DIR, subDirName);
    subDir.mkdirs();
    return subDir;
  }

}
