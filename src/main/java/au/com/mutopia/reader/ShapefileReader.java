package au.com.mutopia.reader;

import au.com.mutopia.util.FileUtils;
import au.com.mutopia.util.ZipUtils;
import com.google.common.io.Files;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

@Log4j
public class ShapefileReader {
  private final GeometryFactory geometryFactory = new GeometryFactory();
  private final ZipUtils zipUtils = new ZipUtils();
  private final FileUtils fileUtils = new FileUtils();
  private final WKTReader wktReader = new WKTReader();

  private static final String GEOMETRY_ATTRIBUTE = "the_geom";
  private static final String REF_LAT_ATTRIBUTE = "ref_lat";
  private static final String REF_LON_ATTRIBUTE = "ref_lon";
  private static final String SPACE_ID = "SPACEID";

  private double lowestXOffSet = Double.MAX_VALUE;
  private double lowestYOffSet = Double.MAX_VALUE;

  private static final double siteXOffset = 333161.0;
  private static final double siteYOffset = 6248917.0;
  private static Point siteRefLatLong;

  private File shapefile;

  public List<Map<String, String>> getFeatures(File file) throws IOException {
    String fileExtension = Files.getFileExtension(file.getName());
    if(fileExtension.equals("zip")) {
      List<File> unzippedFiles = zipUtils.unzip(file, fileUtils.createTempDir());
      for (File unzippedFile : unzippedFiles) {
        if (Files.getFileExtension(unzippedFile.getName()).equals("shp")) {
          this.shapefile = unzippedFile;
          break;
        }
      }
    } else {
      this.shapefile = file;
    }

    siteRefLatLong = getDefaultLatLongOffSet();

    List<String> columnHeaders = getColumnHeaders();
    if (columnHeaders == null) {
      return null;
    }
    columnHeaders.remove(GEOMETRY_ATTRIBUTE);

    List<Map<String, String>> features = new ArrayList<>();
    for (SimpleFeature feature : this.getFeatures()) {
      features.add(getFeatureMap(feature, columnHeaders));
    }
    return features;
  }

  /**
   * Creates and gets the geo object representing the Shapefile feature.
   *
   * @param feature The Shapefile feature to be converted into geo object.
   * @param columnHeaders The list of column headers for the Shapefile feature.
   * @return the geo object representing the Shapefile feature.
   */
  private Map<String, String> getFeatureMap(SimpleFeature feature, List<String> columnHeaders) {
    Map<String, String> features = new HashMap<>();
    features.putAll(getNonGeometricAttributeMap(feature, columnHeaders));
    try {
      features.put(GEOMETRY_ATTRIBUTE, writeGeometricValues(feature));
      features.put(REF_LON_ATTRIBUTE, Double.toString(siteRefLatLong.getX()));
      features.put(REF_LAT_ATTRIBUTE, Double.toString(siteRefLatLong.getY()));
    } catch (ParseException e) {
      log.error("Error parsing geometry.");
    }

    return features;
  }

  private Point getDefaultLatLongOffSet() {
    Point offset = geometryFactory.createPoint(new Coordinate(siteXOffset, siteYOffset));
    try {
      CoordinateReferenceSystem gda94CRS = getGDA94CoordinateReferenceSystem();
      CoordinateReferenceSystem defaultCRS = getDefaultCoordinateReferenceSystem();
      MathTransform transform = CRS.findMathTransform(gda94CRS, defaultCRS, true);
      return (Point) JTS.transform(offset, transform);
    } catch (FactoryException | TransformException e) {
      return geometryFactory.createPoint(new Coordinate(0, 0));
    }
  }

  private String writeGeometricValues(SimpleFeature feature) throws ParseException {
    Object defaultGeometry = feature.getDefaultGeometry();
    if (defaultGeometry instanceof GeometryCollection) {
      GeometryCollection geometryCollection = (GeometryCollection) defaultGeometry;
      int numOfElements = geometryCollection.getNumGeometries();
      for (int i = 0; i < numOfElements; i++) {
        return getGeometryString(geometryCollection.getGeometryN(i));
      }
    } else {
      return getGeometryString((Geometry) defaultGeometry);
    }
    return null;
  }

  private String getGeometryString(Geometry geometry) throws ParseException {
    Geometry read = wktReader.read(geometry.toText());
    return geometry.toText();
  }

  private Map<String, String> getNonGeometricAttributeMap(SimpleFeature feature,
      List<String> columnHeaders) {
    Map<String, String> attributes = new HashMap<>();
    for (String header : columnHeaders) {
      Object attribute = feature.getAttribute(header);
      if(attribute != null) {
        String value = attribute.toString();
        attributes.put(header, value);
      }
    }
    return attributes;
  }

  /**
   * Gets the feature source from the shapefile
   */
  private SimpleFeatureSource getFeatureSource() {
    FileDataStore store = null;
    try {
      store = FileDataStoreFinder.getDataStore(shapefile);
      return store.getFeatureSource();
    } catch (Exception e) {
      log.error(e);
      log.error("Shapefile Reader : Error getting feature source from File Data Store.");
      return null;
    } finally {
      if (store != null) {
        store.dispose();
      }
    }
  }

  /**
   * Gets the coordinate reference system of the shapefile feature source
   *
   * @return coordinate reference system
   */
  private CoordinateReferenceSystem getSourceCRSFromSource(SimpleFeatureSource source) {
    try {
      SimpleFeatureType schema = source.getSchema();
      return schema.getCoordinateReferenceSystem();
    } catch (Exception e) {
      log.error(e);
      log.error("Shapefile Reader : Error getting CRS from Feature Source.");
      return null;
    }
  }

  /**
   * Gets the column headers. However, FeatureID is not included in the list of headers.
   *
   * @return a list of column headers found in the shapefile
   */
  private List<String> getColumnHeaders() {
    if (shapefile == null) {
      return null;
    }
    List<String> colHeaders = new ArrayList<>();
    try {
      SimpleFeatureSource featureSource = getFeatureSource();
      SimpleFeatureCollection features = featureSource.getFeatures();
      for (AttributeDescriptor attribute : features.getSchema().getAttributeDescriptors()) {
        colHeaders.add(attribute.getLocalName());
      }
      return colHeaders;
    } catch (Exception e) {
      log.error(e);
      log.error("Shapefile Reader : Error reading shapefile contents");
      return null;
    }
  }

  /**
   * Gets a list of all features from the shapefile.
   *
   * @return a list of all features
   */
  private List<SimpleFeature> getFeatures() {
    if (shapefile == null) {
      return null;
    }

    List<SimpleFeature> featureList = new ArrayList<>();
    try {
      SimpleFeatureSource featureSource = getFeatureSource();
      SimpleFeatureCollection features = featureSource.getFeatures();
      SimpleFeatureIterator iterator = features.features();
      CoordinateReferenceSystem sourceCRS = getSourceCRSFromSource(featureSource);
      CoordinateReferenceSystem gda94CRS = getGDA94CoordinateReferenceSystem();
      MathTransform transform = CRS.findMathTransform(sourceCRS, gda94CRS, true);

      try {
        while (iterator.hasNext()) {
          try {
            SimpleFeature feature = iterator.next();
            try {
              if (sourceCRS != null) {
                // convert and transform coordinates
                Geometry sourceGeom = (Geometry) feature.getDefaultGeometry();
                Geometry gda94Geom = JTS.transform(sourceGeom, transform);
                for (Coordinate coordinate : gda94Geom.getCoordinates()) {
                  if (coordinate.x < lowestXOffSet) lowestXOffSet = coordinate.x;
                  if (coordinate.y < lowestYOffSet) lowestYOffSet = coordinate.y;
                  coordinate.x -= 333161.0;
                  coordinate.y -= 6248917.0;
                }
                feature.setDefaultGeometry(gda94Geom);
              }
            } catch (Exception e) {
              log.error(e);
            }
            featureList.add(feature);
          } catch (RuntimeException re) {
            log.error(re);
            log.error("Error retrieving features from shapefile");
            return null;
          }
        }
      } finally {
        iterator.close();
      }
    } catch (Exception e) {
      log.error(e);
      log.error("Shapefile Reader : Error reading shapefile contents");
      return null;
    }
    return featureList;
  }

  private CoordinateReferenceSystem getGDA94CoordinateReferenceSystem() throws FactoryException {
    CoordinateReferenceSystem coordinateReferenceSystem;
    CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
    coordinateReferenceSystem = factory.createCoordinateReferenceSystem("EPSG:28356");
    return coordinateReferenceSystem;
  }

  private CoordinateReferenceSystem getDefaultCoordinateReferenceSystem() throws FactoryException {
    CoordinateReferenceSystem coordinateReferenceSystem;
    CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
    coordinateReferenceSystem = factory.createCoordinateReferenceSystem("EPSG:4326");
    return coordinateReferenceSystem;
  }
}
