package au.com.mutopia.converter;

import au.com.mutopia.util.BimServerAuthenticator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.bimserver.client.BimServerClient;
import org.bimserver.interfaces.objects.SDeserializerPluginConfiguration;
import org.bimserver.interfaces.objects.SProject;
import org.bimserver.interfaces.objects.SSerializerPluginConfiguration;
import org.bimserver.shared.PublicInterfaceNotFoundException;
import org.bimserver.shared.exceptions.ServiceException;

public class IfcConverter {
  private final String host = "http://localhost:8082";
  private final String username = "admin@bimserver.org";
  private final String password = "admin";
  private final BimServerAuthenticator bimServerAuthenticator = new BimServerAuthenticator();

  /**
   * The name of serializer and deserializer plugin.
   */
  private static String JSON_IFC_GEOM_TREE_DESERIALIZER_NAME = "PimFeatureToIfcDeserializer";
  private static String IFC_STEP_SERIALIZER_NAME = "Ifc2x3";

  /**
   * Bim Server client to access BimServer's API.
   */
  private BimServerClient bimServerClient;

  /**
   * Gets Json Geometry string from BimServer. Note: BimServer must be running for geometry
   * serializer to work. material color, parameters and object hierarchy.
   *
   * @param ifcFile The IFC file to be converted.
   * @return Json Geometry string converted from the IFC file.
   */
  public byte[] convertJsonToIfc(File ifcFile) {
    try {
      bimServerClient = bimServerAuthenticator.connectToBimServer(username, password, host);

      // Create a temporary project to upload the IFC file to.
      SProject newProject =
          bimServerClient.getBimsie1ServiceInterface().addProject("test" + Math.random());

      // Find the most suitable deserializer for IFC.
      SDeserializerPluginConfiguration deserializer =
          bimServerClient.getBimsie1ServiceInterface()
              .getDeserializerByName(JSON_IFC_GEOM_TREE_DESERIALIZER_NAME);

      // Checkin - upload file to project.
      bimServerClient
          .checkin(newProject.getOid(), "test", deserializer.getOid(), false, true, ifcFile);

      SSerializerPluginConfiguration jsonIfcGeomSerializer =
          bimServerClient.getBimsie1ServiceInterface()
              .getSerializerByName(IFC_STEP_SERIALIZER_NAME);
      if (jsonIfcGeomSerializer == null) {
        return null;
      }

      // Find a serializer plugin.
      // Currently using JsonGeometry because it does most of what is required, new plugin is required to
      // extract more specific data.
      SSerializerPluginConfiguration jsonGeometrySerializerSerializer = jsonIfcGeomSerializer;

      // Get the project details, seems redundant but it makes the api works.
      // Maybe it is required to get the latest 'image' of the project from BimServer.
      newProject =
          bimServerClient.getBimsie1ServiceInterface().getProjectByPoid(newProject.getOid());

      // Download the latest revision  (the one we just checked in).
      // Convert the data to Json string.
      Long downloadId = bimServerClient.getBimsie1ServiceInterface()
          .download(newProject.getLastRevisionId(), jsonGeometrySerializerSerializer.getOid(), true,
              false); // Note: sync: false
      InputStream downloadData =
          bimServerClient.getDownloadData(downloadId, jsonGeometrySerializerSerializer.getOid());

      bimServerClient.getBimsie1ServiceInterface().deleteProject(newProject.getOid());

      byte[] result = IOUtils.toByteArray(downloadData);
      return result;
    } catch (PublicInterfaceNotFoundException | ServiceException | IOException e) {
    }

    return null;
  }
}
