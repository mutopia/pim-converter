package au.com.mutopia.converter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FilenameUtils;

@Log4j
public class ShpConverter extends JFrame {
  private ShapefileToIfcConverter shapefileToIfcConverter = new ShapefileToIfcConverter();

  private JList fileList;
  private DefaultListModel<String> listModel;
  private JButton resetButton;
  private JButton convertButton;
  private JButton saveButton;

  private JFileChooser fileChooser;
  private JFileChooser saveFileChooser;
  private List<File> loadedFiles = new ArrayList<>();
  private byte[] bytes;

  private static final String CONVERT_ALL = "Convert All.";
  private static final String CONVERT_SELECTED = "Convert Selected.";

  private static final String SHP_EXTENSION = "shp";
  private static final String IFC_EXTENSION = "ifc";

  public static void main(String[] args) throws Exception {
    JFrame frame = new ShpConverter();
    frame.setSize(500, 250);
    frame.setVisible(true);
  }

  public ShpConverter() {
    JMenuBar menubar = new JMenuBar();
    setJMenuBar(menubar);

    JMenu fileMenu = new JMenu("File");
    menubar.add(fileMenu);

    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    fileList = new JList();
    listModel = new DefaultListModel<>();
    fileList.setModel(listModel);
    JScrollPane scrollPane = new JScrollPane(fileList);
    contentPane.add(scrollPane, BorderLayout.CENTER);

    JPanel buttonPane = new JPanel();

    buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
    buttonPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    buttonPane.add(Box.createHorizontalGlue());
    resetButton = new JButton("Reset");
    resetButton.setEnabled(false);
    resetButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        reset();
      }
    });
    buttonPane.add(resetButton);
    buttonPane.add(Box.createRigidArea(new Dimension(5,0)));
    convertButton = new JButton(CONVERT_ALL);
    convertButton.setEnabled(false);
    fileList.addListSelectionListener(new ListSelectionListener() {
      @Override public void valueChanged(ListSelectionEvent e) {
        if (fileList.getSelectedIndices().length == 0) {
          convertButton.setText(CONVERT_ALL);
        } else {
          convertButton.setText(CONVERT_SELECTED);
        }
      }
    });
    convertButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        convertShpToIfc();
      }
    });
    buttonPane.add(convertButton);
    buttonPane.add(Box.createRigidArea(new Dimension(5,0)));
    saveButton = new JButton("Save As IFC");
    saveButton.setEnabled(false);
    saveButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        saveIfc();
      }
    });
    buttonPane.add(saveButton);
    contentPane.add(buttonPane, BorderLayout.PAGE_END);

    getRootPane().setDefaultButton(convertButton);

    pack();

    fileMenu.add(new AbstractAction("Add Shapefile..."){
      @Override
      public void actionPerformed(ActionEvent arg0) {
        try {
          addShp();
        } catch (Exception e) {
          log.error("Error adding Shapefile.", e);
        }
      }
    });
    fileMenu.addSeparator();
    fileMenu.add(new AbstractAction("Exit"){
      @Override
      public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
      }
    });

    fileChooser = new JFileChooser();
    FileFilter shpFilter = new FileNameExtensionFilter("Shapefile", SHP_EXTENSION);
    fileChooser.setFileFilter(shpFilter);
    fileChooser.setMultiSelectionEnabled(true);

    saveFileChooser = new JFileChooser() {
      @Override
      public void approveSelection() {
        File f = getSelectedFile();
        if (f.exists() && getDialogType() == SAVE_DIALOG) {
          int result = JOptionPane.showConfirmDialog(this, "The file exists, overwrite?",
              "Existing file", JOptionPane.YES_NO_CANCEL_OPTION);
          switch (result) {
            case JOptionPane.YES_OPTION:
              super.approveSelection();
              return;
            case JOptionPane.NO_OPTION:
              return;
            case JOptionPane.CLOSED_OPTION:
              return;
            case JOptionPane.CANCEL_OPTION:
              cancelSelection();
              return;
          }
        }
        super.approveSelection();
      }
    };
    FileFilter ifcFilter = new FileNameExtensionFilter("IFC", IFC_EXTENSION);
    saveFileChooser.setFileFilter(ifcFilter);
    saveFileChooser.setMultiSelectionEnabled(false);

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void addShp() throws Exception {
    int returnVal = fileChooser.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File[] files = fileChooser.getSelectedFiles();
      for (File file : files) {
        loadedFiles.add(file);
        listModel.addElement(shortenFileName(file));
        fileChooser.setCurrentDirectory(file.getParentFile());
        saveFileChooser.setCurrentDirectory(file.getParentFile());
      }
      convertButton.setEnabled(true);
    } else {
    }
  }

  private void reset() {
    listModel.clear();
    loadedFiles.clear();
    resetButton.setEnabled(false);
    convertButton.setEnabled(false);
    saveButton.setEnabled(false);
  }

  private void convertShpToIfc() {
    List<File> filesToConvert = new ArrayList<>();
    if (convertButton.getText().equals(CONVERT_SELECTED)) {
      for (int i : fileList.getSelectedIndices()) {
        filesToConvert.add(loadedFiles.get(i));
      }
    } else {
      filesToConvert = loadedFiles;
    }

    convertButton.setEnabled(false);

    try {
      bytes = shapefileToIfcConverter.convertShapefilesToIfc(filesToConvert);
      saveButton.setEnabled(true);
    } catch (IOException e) {
      log.error("Error converting to IFC with BIM Server.", e);
    }
  }

  private void saveIfc() {
    int returnVal = saveFileChooser.showSaveDialog(this);
    saveFileChooser.setDialogTitle("Save Converted IFC As:");

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = getFileWithIfcExtension(saveFileChooser.getSelectedFile());

      try {
        FileOutputStream out = new FileOutputStream(file);
        out.write(bytes);
        out.flush();
        out.close();
        convertButton.setEnabled(true);
        resetButton.setEnabled(true);
      } catch (IOException e) {
        log.error("Error converting to IFC.", e);
      }
    } else {
    }
  }

  private String shortenFileName(File file) {
    return file.getParentFile().getName() + "/" + file.getName();
  }

  private File getFileWithIfcExtension(File file) {
    if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase(IFC_EXTENSION)) {
      return new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()) + "." +
          IFC_EXTENSION);
    }
    return file;
  }
}
