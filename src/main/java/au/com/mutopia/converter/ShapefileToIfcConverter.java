package au.com.mutopia.converter;

import au.com.mutopia.reader.ShapefileReader;
import au.com.mutopia.util.FileUtils;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;

public class ShapefileToIfcConverter {
  private final ShapefileReader reader = new ShapefileReader();
  private final IfcConverter ifcConverter = new IfcConverter();
  private final ObjectMapper mapper = new ObjectMapper();
  private final FileUtils fileUtils = new FileUtils();

  public byte[] convertShapefilesToIfc(List<File> files) throws IOException {
    File shapefileDataAsJson = getShapefileDataAsJson(files);
    return ifcConverter.convertJsonToIfc(shapefileDataAsJson);
  }

  public File getShapefileDataAsJson(List<File> files) throws IOException {
    List<Map<String, String>> features = Lists.newArrayList();
    for (File file : files) {
       features.addAll(reader.getFeatures(file));
    }
    String jsonString = mapper.writeValueAsString(features);
    File jsonFile = fileUtils.createTemporaryFileWithContent("convertedShapefileJson.json",
        jsonString.getBytes());

    return jsonFile;
  }
}
